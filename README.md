# Apache Log Convert to CSV

Convert Apache common logs and combined log formats to CSV format with an additional 
ID column for data analysis

## Getting Started

Just have to make sure Python is installed on your system then from command line use 
(Windows) python log_converter.py -f yourfilename and a converted file will be created
called out.csv and will replace any previous out.csv file.

### Prerequisites

Python 2.6 or higher (could be lower haven't tested)

### Installing

Download or pull log_converter.py and place in desired directory.

## Running the tests

No tests written yet.

### Break down into end to end tests

None yet.

## Built With

Pycharm 2018

## Contributing

James Caldwell

## Versioning

0.1.2
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

James Caldwell


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

